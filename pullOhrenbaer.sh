#!/bin/bash

function grepStrings() {
	awk -v RS="<article" -v FS="\n" ' /komplette/ &&  NR>1 {for (i = 1; i <= NF; i++) { if ( $i ~ "https:" || $i ~ /class="manualteasertitle"/ ){ print $i }} print "#######\n" }' ${1}
}

function generateDownloadCommand() {
	awk -v RS="#######" '{ 
				split($0,elements,/<|>| \||href=|target=/) ; 
				gsub(" ","_",elements[3]); 
				gsub("\"","",elements[8]); 
				if(elements[8] != "") {printf("%s.mp3 # %s \n",elements[3], elements[8]) } 
			    }' ${1}

}

url="https://www.ohrenbaer.de/podcast/podcast.html"
downloadFolder="${HOME}/Nextcloud/jtshare/080_Karlotta/Ohrenbaer"
mkdir -p ${downloadFolder}

dataPairs=$(curl -s ${url}| grepStrings)
IFS="#"
echo "$dataPairs" | generateDownloadCommand | while read filename downloadUrl
do
	unset absoluteFilename
	absoluteFilename=$( echo "${downloadFolder}/${filename}" | tr -d " ")
	cleanUrl=$( echo "${downloadUrl}" | tr -d ' ' )

	[[ ! -e "${absoluteFilename}" ]] && { echo "# found new stories: $absoluteFilename"; wget -O ${absoluteFilename} ${cleanUrl} ; }   
	#[[ ! -f $downloadFolder/$filename ]] && { echo "not found: " ; }
	# echo "# wget -O ${downloadFolder}/${filename} $downloadUrl "

done
